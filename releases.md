# v2.5.8
[BUGFIX]  Correction d'un bug sur les transporteurs de certaines commandes eshop  
[FEATURE] Amélioration de la personnalisation du tunnel de désabonnement  
[FEATURE] Amélioration de la vitesse d'export des abonnés actifs  
[BUGFIX]  Mise à jour des transporteurs sur les abonnements

# v2.5.7
[BUGFIX] Correction d'un bug sur les transporteurs de certaines commandes eshop  
[FEATURE] Ajout de l'export des abonnés en période de grâce  
[BUGFIX] Correction d'un bug sur le téléchargement de certaines factures  
[BUGFIX] Meilleur affichage du type de carte dans le dashoard  
[FEATURE] Possibilité de bloquer les gros coupons (première commande) pour qu'un utilisateur ne puisse bénéficier d'une offre d'inscription à plusieurs reprises   
[FEATURE] Optimisation des exports Abonnements, Expéditions, Transactions, Actifs

# v2.5.6
[BUGFIX] Amélioration du tracking Affilae
[FEATURE] Ajout du tracking ecommerce analytics, sur les commandes d'abonnement, eshop, et carte cadeau  
[FEATURE] Ajout de la gestion de pages virtuel pour la gestion funel + actions des tunnels d'achat
* type: 'action', name: 'Plan selected: {NOM_FORMULE}'
* type : 'pageview', url: 'select-profile.html'
* type : 'pageview', url: 'select-plan.html'
* type : 'pageview', url: 'logged-out-form.html'
* type : 'pageview', url: 'logged-in-form.html'
* type : 'pageview', url: 'credit-card.html'
* type : 'pageview', url: 'order-confirmation.html'
* type : 'pageview', url: 'user-registred.html'
* type: 'action', name: 'Select profile : {VALEUR}'
* type : 'pageview', url: 'order-gift.html'
* type: 'action', name: 'Gift seleted : {NAME}'
* type : 'pageview', url: 'ordered-gift.html'

[FEATURE] Ajout des events dans le tunnel de désabonnement: 
* type: 'action', name: 'Unsub : select_pause'
* type: 'action', name: 'Unsub : select_break'
* type: 'action', name: 'Unsub : break_validated'
* type: 'action', name: 'Unsub : go_type'
* type: 'action', name: 'Unsub : pause_months_confirmed'
* type: 'action', name: 'Unsub : accept_voucher'
* type: 'action', name: 'Unsub : decline_voucher'

[FEATURE] Ajout de la liaison avec Mixpanel. Le simple ajout du code de tracking dans Google Tag Manager suffit.  
[BUGFIX] Amélioration du tunnel de désabonnement.  
[BUGFIX] Suppression de date de rebill pour les engagments qui expirent

# V2.5.5
* [BUGFIX] Sur les profils en réponse type Oui/Non
* [BUGFIX] Afffichage sur les adresses
* [BUGFIX] Notification bloquées pour les engagements qui arrivent à expiration
* [UPDATE] Mise à jour vers la dernière version de font-awesome
* [FEATURE] Amélioration des notifications du dashboard
* [FEATURE] Notification à l'équipe en cas d'echec de paiement
* [BUGFIX] Correction du filtre : Réactivations à vérifier
* [BUGFIX] Coupons de réactivation ne sont plus en centimes

# v2.5.4

* Amélioration de la présentation de profils sur le tunnel d'achat
* Amélioration de la gestion des textes sur la popup d'identification
* Amélioration des performances de l'eshop
* Correction d'un bug de selection sur les profils
* Ajout des formules avec engagement minimum
* Amélioration de la synchronisation avec Mailchimp
* Correction d'un bug sur la personnalisation du tunnel de désabonnement
* Amélioration de l'affichage des prix
* Amélioration de la génération des cartes cadeau (suppression des caractères oOlLiI)
* Optimisation du temps de chargement de la page d'accueil

# v2.5.3

* Amélioration de la présentation des formules avec illustration
* Possibilité de bloquer la mise en pause avant récéption de la première box
* Ajout de notifications mail de relance pour les paniers abandonnés
* Mise à jour vers Bootstrap 4.1.3

# v2.5.2

* Ajout d'un outils de notification et d'annonces depuis le dashboard
* Création d'un gestionnaire de blog, avec :
  * Billets
  * Catégories
  * Dossiers
* Mise à disposition de contenus relatifs à la loi RGPD
* Améliioration des commentaires

# v2.5.1

* Ajout du sitemap, depuis le panneau de configuration du site
* Ajout d'outils statistiques:
    * Estimation des expéditions
    * Expéditions estimées
    * Chiffre d'affaire : vue d'ensemble
    * Impayés
    * Revenus recurents mensuels (MRR)
    *  Activité journalière
    *  Taux de refus par type de carte
    *  Revenu moyen par client
    *  Churn
    *  Récap mensuel
    *  Balance mensuelle
    *  Balance jounalière
    *  Volume par type de carte

* Ajout d'une icone de chargement sur les boutons de validation de commande

